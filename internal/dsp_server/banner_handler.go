package dspserver

import (
	"fmt"
	"math"
	"math/rand"
	"time"

	"github.com/gofrs/uuid"
	jsoniter "github.com/json-iterator/go"
	"github.com/mxmCherry/openrtb/v15/openrtb2"
	"github.com/valyala/fasthttp"
	"gitlab.com/rapidcodelab-opensource/fake-dsp/pkg/helpers"
	rtbvalidators "gitlab.com/rapidcodelab-opensource/fake-dsp/pkg/rtb_validators"
)

var banners = []string{
	"https://banners.rapidcodelab.repl.co/banners/1.jpg",
	"https://banners.rapidcodelab.repl.co/banners/2.jpg",
	"https://banners.rapidcodelab.repl.co/banners/3.jpg",
	"https://banners.rapidcodelab.repl.co/banners/4.jpg",
	"https://banners.rapidcodelab.repl.co/banners/5.gif",
	"https://banners.rapidcodelab.repl.co/banners/6.jpg",
	"https://banners.rapidcodelab.repl.co/banners/8.gif",
	"https://banners.rapidcodelab.repl.co/banners/9.jpg",
	"https://banners.rapidcodelab.repl.co/banners/10.jpg",
	"https://banners.rapidcodelab.repl.co//banners/8.jpg",
}

var json = jsoniter.ConfigCompatibleWithStandardLibrary

func (s *server) bannerHandler(ctx *fasthttp.RequestCtx) {
	var err error

	rand.Seed(time.Now().UnixNano())

	bidRequest := openrtb2.BidRequest{}
	err = json.Unmarshal(ctx.Request.Body(), &bidRequest)
	if err != nil {
		s.logger.Warn(err)
		ctx.Response.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}

	err = rtbvalidators.ValidateOpenRTB25BannerBidRequest(&bidRequest)
	if err != nil {
		s.logger.Warn(err)
		ctx.Response.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.SetBody([]byte(err.Error()))
		return
	}

	bids := []openrtb2.Bid{}
	//make bids
	for i := 0; i < len(bidRequest.Imp); i++ {
		bid := openrtb2.Bid{}

		u, err := uuid.NewV4()
		if err != nil {
			s.logger.Warn(err)
		}

		bid.ID = u.String()

		bid.ImpID = bidRequest.Imp[i].ID
		bid.AdM = fmt.Sprintf("<a href=\"https://ya.ru\"><img src=\"%s\"/></a>", banners[i])

		bid.Price = math.Round(helpers.RandFloat(1.1, 3.05)*100) / 100
		bids = append(bids, bid)
	}

	bidResponse := openrtb2.BidResponse{}

	bidResponse.ID = bidRequest.ID

	seatBid := openrtb2.SeatBid{
		Bid: bids,
	}

	bidResponse.SeatBid = append(bidResponse.SeatBid, seatBid)

	bidResponseJSON, err := json.Marshal(bidResponse)
	if err != nil {
		s.logger.Warn(err)
		ctx.Response.SetStatusCode(fasthttp.StatusBadGateway)
		return
	}

	//send response
	ctx.Response.Header.SetContentType("application/json")
	ctx.Response.SetStatusCode(fasthttp.StatusOK)
	ctx.Response.SetBody(bidResponseJSON)

}
