package dspserver

import (
	"math"

	"github.com/gofrs/uuid"
	"github.com/mxmCherry/openrtb/v15/native1/request"
	"github.com/mxmCherry/openrtb/v15/native1/response"
	"github.com/mxmCherry/openrtb/v15/openrtb2"
	"github.com/valyala/fasthttp"
	"gitlab.com/rapidcodelab-opensource/fake-dsp/pkg/helpers"
	rtbvalidators "gitlab.com/rapidcodelab-opensource/fake-dsp/pkg/rtb_validators"
)

var titles = []string{
	"Wordpress Hosting",
	"Electronic Parts Store",
	"Get Free BTC",
	"Domain Hosting Indonesia",
	"Wiggins Hair",
	"Indonesia Motor Show",
	"Mavic 2",
	"Free SEO Audit",
	"Banner Templates",
	"Q Miliar Pertama",
}

var descs = []string{
	"Cheap wordpress hosting at turhost.com",
	"USA Electronic component distributer",
	"BitShark.io - get free bitcoins every hour",
	"IDwebhost.com - hosting mulai 1.000/bln",
	"Up to 51% off - shop now",
	"9 -19 April 2020 - International Motor Show",
	"In stock all Mavic models",
	"SEO Advantage - Rank your website #1 in google",
	"Template promosi yang siap meledakkan",
	"Dari Facebook + Toko Online",
}

func (s *server) nativeHandler(ctx *fasthttp.RequestCtx) {
	var err error

	bidRequest := openrtb2.BidRequest{}
	err = json.Unmarshal(ctx.Request.Body(), &bidRequest)
	if err != nil {
		s.logger.Warn(err)
		ctx.Response.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}

	err = rtbvalidators.ValidateOpenRTB25NativeBidRequest(&bidRequest)
	if err != nil {
		s.logger.Warn(err)
		ctx.Response.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.SetBody([]byte(err.Error()))
		return
	}

	bids := []openrtb2.Bid{}

	for i := 0; i < len(bidRequest.Imp); i++ {

		//native part
		nativeAdRequestObject := request.Request{}

		err = json.Unmarshal([]byte(bidRequest.Imp[i].Native.Request), &nativeAdRequestObject)
		if err != nil {
			s.logger.Error(err)
			continue
		}

		err = rtbvalidators.ValidateOpenRTB25NativeAdRequestObject(&nativeAdRequestObject)
		if err != nil {
			s.logger.Error(err)
			continue
		}

		responseAssets := []response.Asset{}

		for ix := 0; ix < len(nativeAdRequestObject.Assets); ix++ {

			asset := response.Asset{}
			asset.ID = &nativeAdRequestObject.Assets[ix].ID
			asset.Required = nativeAdRequestObject.Assets[ix].Required

			if nativeAdRequestObject.Assets[ix].Title != nil {
				asset.Title = &response.Title{
					Text: titles[i],
				}
			}
			if nativeAdRequestObject.Assets[ix].Img != nil {
				asset.Img = &response.Image{
					URL: banners[i],
				}
			}

			if nativeAdRequestObject.Assets[ix].Data != nil {
				asset.Data = &response.Data{
					Value: descs[i],
				}
			}

			responseAssets = append(responseAssets, asset)
		}

		nativeAdResponseObject := response.Response{}
		nativeAdResponseObject.Ver = "1.2"
		nativeAdResponseObject.Link = response.Link{
			URL: "https://ya.ru",
		}

		nativeAdResponseObject.Assets = responseAssets
		//more detailt to native response

		nativeAdResponseObjectJSON, err := json.Marshal(nativeAdResponseObject)
		if err != nil {
			s.logger.Error(err)
			continue
		}

		bid := openrtb2.Bid{}
		u, err := uuid.NewV4()
		if err != nil {
			s.logger.Warn(err)
		}
		bid.ID = u.String()
		bid.ImpID = bidRequest.Imp[i].ID
		bid.AdM = string(nativeAdResponseObjectJSON)

		bid.Price = math.Round(helpers.RandFloat(1.1, 3.05)*100) / 100
		bids = append(bids, bid)

	}

	bidResponse := openrtb2.BidResponse{}

	bidResponse.ID = bidRequest.ID

	seatBid := openrtb2.SeatBid{
		Bid: bids,
	}

	bidResponse.SeatBid = append(bidResponse.SeatBid, seatBid)

	bidResponseJSON, err := json.Marshal(bidResponse)
	if err != nil {
		s.logger.Warn(err)
		ctx.Response.SetStatusCode(fasthttp.StatusBadGateway)
		return
	}

	//send response
	ctx.Response.Header.SetContentType("application/json")
	ctx.Response.SetStatusCode(fasthttp.StatusOK)
	ctx.Response.SetBody(bidResponseJSON)

}
