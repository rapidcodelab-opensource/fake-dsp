package dspserver

import (
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/buaazp/fasthttprouter"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/reuseport"
	"gitlab.com/rapidcodelab-opensource/fake-dsp/internal/config"
	"gitlab.com/rapidcodelab-opensource/fake-dsp/pkg/logging"
)

const (
	serverName = "RapidCodeLab Fake DSP"
)

type server struct {
	logger *logging.Logger
	config *config.Config
	http   *fasthttp.Server
}

func New(logger *logging.Logger) server {

	//config
	config := config.GetConfig()

	return server{
		logger: logger,
		config: config,
		http: &fasthttp.Server{
			Name: serverName,
		},
	}

}

func (s *server) Run() {

	//metrics for Prometheus
	go func() {
		http.Handle("/metrics", promhttp.Handler())
		err := http.ListenAndServe(s.config.MetricsListenAddr, nil)
		s.logger.Warn(err)
	}()

	//routes
	router := fasthttprouter.New()
	router.POST("/openrtb/banner", s.bannerHandler)
	router.POST("/openrtb/native", s.nativeHandler)

	//create server
	s.http.Handler = router.Handler

	//listener
	listener, err := reuseport.Listen(s.config.ListenNetwork, s.config.ListenAddr)
	if err != nil {
		s.logger.Fatal(err)
	}

	//graceful stop
	var gracefulStop = make(chan os.Signal, 1)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)

	go func() {
		s.logger.Info("Started graceful stop goroutine")
		sig := <-gracefulStop
		s.logger.Info("Sig graceful stop server", sig)

		err := listener.Close()
		if err != nil {
			s.logger.Warn(err)
		}
	}()

	//start serving...
	s.logger.Info("RapidCodeLab Fake DSP started...")
	if err := s.http.Serve(listener); err != nil {
		s.logger.Fatal(err)
	}
}
