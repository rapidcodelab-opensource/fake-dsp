package config

import (
	"sync"

	"github.com/ilyakaznacheev/cleanenv"
	"gitlab.com/rapidcodelab-opensource/fake-dsp/pkg/logging"
)

type Config struct {
	DebugMode         int    `env:"DEBUG_MODE"`
	ListenNetwork     string `env:"LISTEN_NETWORK" env-default:"tcp4"`
	ListenAddr        string `env:"LISTEN_ADDR" env-default:":8090"`
	MetricsListenAddr string `env:"METRICS_LISTEN_ADDR" env-default:":9090"`
}

var instance *Config
var once sync.Once

func GetConfig() *Config {
	once.Do(func() {

		logger := logging.Getlogger()

		instance = &Config{}
		err := cleanenv.ReadEnv(instance)
		if err != nil {
			help, _ := cleanenv.GetDescription(instance, nil)
			logger.Warn(help)
			logger.Fatal(err)
		}

	})
	return instance
}
