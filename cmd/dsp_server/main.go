package main

import (
	dspserver "gitlab.com/rapidcodelab-opensource/fake-dsp/internal/dsp_server"
	"gitlab.com/rapidcodelab-opensource/fake-dsp/pkg/logging"
)

func main() {
	l := logging.Getlogger()
	s := dspserver.New(&l)
	s.Run()
}
