include .env
DSP_SERVER_BINARY_NAME=server


build:
	go build -o .bin/${DSP_SERVER_BINARY_NAME} cmd/dsp_server/main.go
run:
	go build -o .bin/${DSP_SERVER_BINARY_NAME} cmd/dsp_server/main.go
	chmod +x .bin/${DSP_SERVER_BINARY_NAME}
	./.bin/${DSP_SERVER_BINARY_NAME}
clean:
	go clean
	rm .bin/${DSP_SERVER_BINARY_NAME}    
