package rtberrors

const (
	RTBErrorWrongBidRequestID           = "BidRequest object missed required field \"id\""
	RTBErrorWrongBidRequestImpObjects   = "BidRequest object missed required \"imp\" or it has no Imp object in it."
	RTBErrorWrongBidRequestDeviceObject = "BidRequest object mised required field \"device\""
	RTBErrorWrongBidRequestSiteObject   = "BidRequest object mised required field \"site\""
	RTBErrorWrongBidRequestUserObject   = "BidRequest object mised required field \"user\""
)
